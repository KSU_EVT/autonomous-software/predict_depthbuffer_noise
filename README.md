# Predict Depth Buffer Noise

This program uses stored values from [this document](https://docs.google.com/spreadsheets/d/1ymn-0D4HcCbzYP-iPycj_PIdSwmrLenlGryuZDyA4rQ) to estimate the expected variance of a datapoint from the OAK-D Pro depth camera at an arbitrary depth.

## run me
```bash
me@my_pc % python3 predict_depthbuffer_noise.py
Please enter target distance for which to find variance: 5.3
Calculated Variance: 2.7413640865185243
```

## Future
* Port to C++ for faster runtime
* Integrate with SLAM
