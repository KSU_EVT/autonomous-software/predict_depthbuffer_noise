#!/usr/bin/env python3
import math
from math import sqrt

# data from https://docs.google.com/document/d/1F4Y6S6KtZ4f8RBE4W-o9x6xVXbqsw8UIGWPkML-on1Y/
# based on theoretical pixel disparity at assesed ranges
dist_arr = [0.7037234043, 0.7112903226, 0.7190217391, 0.7269230769, 0.735, 0.743258427, 0.7517045455, 0.7603448276, 0.7691860465, 0.7782352941, 0.7875, 0.7969879518, 0.8067073171, 0.8166666667, 0.826875, 0.8373417722, 0.8480769231, 0.8590909091, 0.8703947368, 0.882, 0.8939189189, 0.9061643836, 0.91875, 0.9316901408, 0.945, 0.9586956522, 0.9727941176, 0.9873134328, 1.002272727, 1.017692308, 1.03359375, 1.05, 1.066935484, 1.08442623, 1.1025, 1.121186441, 1.140517241, 1.160526316, 1.18125, 1.202727273, 1.225, 1.248113208, 1.272115385, 1.297058824, 1.323, 1.35, 1.378125, 1.407446809, 1.438043478, 1.47, 1.503409091, 1.538372093, 1.575, 1.613414634, 1.65375, 1.696153846, 1.740789474, 1.787837838, 1.8375, 1.89, 1.945588235, 2.004545455, 2.0671875, 2.133870968, 2.205, 2.281034483, 2.3625, 2.45, 2.544230769, 2.646, 2.75625, 2.876086957, 3.006818182, 3.15, 3.3075, 3.481578947, 3.675, 3.891176471, 4.134375, 4.41, 4.725, 5.088461538, 5.5125, 6.013636364, 6.615, 7.35, 8.26875, 9.45, 11.025, 13.23, 16.5375, 22.05, 33.075, 66.15]
diff_arr = [0.007407614782, 0.007566918325, 0.00773141655, 0.007901337793, 0.008076923077, 0.008258426966, 0.008446118488, 0.008640282132, 0.008841218925, 0.009049247606, 0.009264705882, 0.009487951807, 0.009719365266, 0.009959349593, 0.01020833333, 0.01046677215, 0.01073515093, 0.01101398601, 0.01130382775, 0.01160526316, 0.01191891892, 0.01224546464, 0.01258561644, 0.01294014085, 0.01330985915, 0.01369565217, 0.01409846547, 0.01451931519, 0.01495929444, 0.01541958042, 0.01590144231, 0.01640625, 0.01693548387, 0.01749074564, 0.01807377049, 0.01868644068, 0.0193308007, 0.02000907441, 0.02072368421, 0.02147727273, 0.02227272727, 0.02311320755, 0.02400217707, 0.02494343891, 0.02594117647, 0.027, 0.028125, 0.02932180851, 0.03059666975, 0.03195652174, 0.03340909091, 0.03496300211, 0.03662790698, 0.03841463415, 0.04033536585, 0.04240384615, 0.04463562753, 0.04704836415, 0.04966216216, 0.0525, 0.05558823529, 0.05895721925, 0.06264204545, 0.06668346774, 0.07112903226, 0.07603448276, 0.08146551724, 0.0875, 0.09423076923, 0.1017692308, 0.11025, 0.1198369565, 0.1307312253, 0.1431818182, 0.1575, 0.1740789474, 0.1934210526, 0.2161764706, 0.2431985294, 0.275625, 0.315, 0.3634615385, 0.4240384615, 0.5011363636, 0.6013636364, 0.735, 0.91875, 1.18125, 1.575, 2.205, 3.3075, 5.5125, 11.025, 33.075]

def main():
    global dist_arr
    global diff_arr

    target_dist = float(input("Please enter target distance for which to find variance: "))

    lowIndex, highIndex = binaryFindNeighbors(target_dist)
    lowDist, highDist = dist_arr[lowIndex], dist_arr[highIndex]
    lowDiff, highDiff = diff_arr[lowIndex], dist_arr[highIndex]

    # interpolate linearly using only two nearest datapoints
    # point slope line formula, solved for y:
    target_diff = ( (highDiff - lowDiff)/(highDist - lowDist) ) * (target_dist - lowDist) + lowDiff
    variance = (target_diff / 2) ** 2 # assume about %98 of data lies within two std. dev. (maxerr/2)**2 is variance
    print(f"Calculated Variance: {variance}")


def binaryFindNeighbors(val_in):
    global dist_arr
    global diff_arr

    # Extrapolate using two most extreme points if OOB
    if (val_in < dist_arr[0]):
        return (0, 1)
    elif (val_in > dist_arr[-1]):
        return (len(dist_arr) - 2, len(dist_arr) - 1)

    L = 0
    R = len(dist_arr) - 1
    while L <= R:
        m = math.floor((L + R) / 2)
        if dist_arr[m] < val_in:
            L = m + 1
        elif dist_arr[m] > val_in:
            R = m - 1
        else:
            # exact match
            return (m, m)
        #if we've broken out of the loop, L > R
        #    meaning that the markers have flipped
        #    so either list[L] or list[R] must be closest to val
        return(R, L)


if __name__ == "__main__":
    main()
